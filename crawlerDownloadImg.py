import pandas as pd
import requests
import shutil
from datetime import datetime
from time import sleep
import os


def log(log_filepath, msg):
    with open(log_filepath, 'a') as f:
        m = datetime.strftime(datetime.now(), '%Y-%m-%d %H:%M:%S') + ': ' + msg + '\n'
        f.write(m)


def download(path_to_csv, save_to, log_filepath='downloadPhotos.log'):
    log(log_filepath, "Started")
    csv = pd.read_csv(path_to_csv)

    print('Total: ', csv.shape[0])
    # index = 1
    if not os.path.exists(save_to):
        os.makedirs(save_to)
    for i, row in csv.iterrows():
        # print '\r' + str(index),
        # index += 1

        r = requests.get(row['previewUrl'], stream=True)
        if r.status_code == 200:
            path = save_to + str(row['gm_id']) + '.jpg'
            with open(path, 'wb') as f:
                r.raw.decode_content = True
                # cv2.imshow('img', r.raw)
                # cv2.waitKey(0)
                f.write(r.content)
                # shutil.copyfileobj(r.raw, f)
        else:
            msg = '%s : GET request failed' % row['previewUrl']
            log(log_filepath, msg)
    log(log_filepath, "Stopped")

# download('2881663.csv')