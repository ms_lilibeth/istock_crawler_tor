#!/usr/bin/env python
# -*- coding: utf-8 -*-
########################################################
#           Python MultiTor
#           Author: RealGame (Tomer Zait)
#           Site:   http://RealGame.co.il
########################################################

#Monkey Patch All - Change Everything To Gevent
from gevent.monkey import patch_all
patch_all()

from TorConfig import *
from gevent import Timeout
from re import findall
from os import makedirs
from time import time as now
from requesocks import request
from stem.process import launch_tor_with_config
from stem.control import Controller
from stem import Signal
from zipfile import ZipFile
from psutil import process_iter, AccessDenied
from subprocess import check_output

#TorConnection - Contains Controller And Subprocess
class TorConnection(object):
    def __init__(self, socksPort, ctrlPort):
        #Variables
        self.__isFree = False
        self.__socksPort = socksPort
        self.__ctrlPort = ctrlPort
        self.__torConfig = None
        self.__torProcess = None
        self.__torCtrl = None
        self.__proxies = None
        self.__lastTimeIpChanged = 0

        #Call Creator
        self.__start()

    def __torPrint(self, line):
        if "Bootstrapped" in line:
            print "%s\t->\t%s" % (self.getId(), line)

    def __open(self):
        #Open Tor Process
        opened = False
        while not opened:
            with Timeout(PROCESS_TIMEOUT, False):
                self.__torProcess = launch_tor_with_config(config=self.__torConfig,
                                                           tor_cmd=TOR_CMD,
                                                           init_msg_handler=self.__torPrint)
                self.__torProcess.stdout.close()
                opened = True
            if not opened:
                self.__torProcess.terminate()
        
        #Open Tor Control
        self.__torCtrl = Controller.from_port(address=HOST, port=self.__ctrlPort)
        self.__torCtrl.authenticate(PASS_PHRASE)
    
    def __start(self):
        #Data Paths
        dataPath = path.join(TOR_ROOT_DATA_PATH, "data_%d" % self.__socksPort)
        if not path.exists(dataPath):
            makedirs(dataPath)
        
        #Create Configuration Dictionary
        self.__torConfig = {"ControlPort": str(self.__ctrlPort),
                            "HashedControlPassword": passPhraseHash,
                            "ExcludeNodes": "{CN},{HK},{MO}",
                            "SOCKSPort": str(self.__socksPort),
                            "DataDirectory": dataPath}

        #Open Tor Process
        self.__open()
        
        #Create Proxy String
        self.__proxies = {"http": "socks5://%s:%d" % (HOST, self.__socksPort),
                          "https": "socks5://%s:%d" % (HOST, self.__socksPort)}

        #The Tor Connection Is Now Ready To Use
        self.__isFree = True

        #Up And Running Message
        print "%s\t->\tUp & Running!" % self.getId()

    def changeState(self):
        self.__isFree = not self.__isFree

    def isFree(self):
        return self.__isFree

    def kill(self):
        with Timeout(TOR_TIMEOUT, False):
            self.__torCtrl.close()
        self.__torProcess.terminate()

    def reset(self):
        #Kill All
        self.kill()

        #Start All
        self.__open()

        #Inform
        print "%s\t->\tUp & Running After Reset!" % self.getId()
        
    def changeIp(self, i, msg):
        #Tor Need 10 Seconds(TOR_TIMEOUT) Difference Between Id Changes
        if (now() - self.__lastTimeIpChanged) >= TOR_TIMEOUT:
            print "%s\t->\t%d) ChangeIP (%s)" % (self.getId(), i, msg)

            #Check If TimedOut
            timedOut = True
            with Timeout(TOR_TIMEOUT, False):
                self.__torCtrl.signal(Signal.NEWNYM)
                timedOut = False
            if timedOut:
                self.reset()

            self.__lastTimeIpChanged = now()
            return True
        return False

    def getId(self):
        return "Tor_%d" % self.__socksPort

    def getProxies(self):
        return self.__proxies

class TorConnectionCollector(object):
    """
    TorConnectionCollector - Sends Free TorConnection To The Thread Function
    """
    def __init__(self):
        self.__torCons = []
        for i in xrange(MAX_NUM_OF_THREADS):
            self.__torCons.append(TorConnection(SOCKS_START_PORT + i, CONTROL_START_PORT + i))

    def getFreeConnection(self):
        while True:
            for conn in self.__torCons:
                if conn.isFree():
                    conn.changeState()
                    return conn

    def killConnections(self):
        for conn in self.__torCons:
            conn.kill()


def pool_function(torRange):
    #Important Variables
    torConn = torConnColl.getFreeConnection()
    proxies = torConn.getProxies()
    torId = torConn.getId()
    size = len(torRange)

    print "%s\t->\tStart (%d - %d)" % (torId, torRange[0], torRange[-1])
    i = 0

    #Using A While Loop - For Loop Cant Move Backwards
    while i < size:
        try:
            #Send Request
            req = request(method="GET",
                          url="http://checkip.dyndns.org/",
                          timeout=REQUEST_TIMEOUT,
                          headers=HEADERS,
                          proxies=proxies)
            res = req.text
            if res == "":
                continue
        except Exception as ex:
            #Change IP
            ipChanged = False
            while not ipChanged:
                ipChanged = torConn.changeIp(torRange[i], ex)
            continue

        #Print Result
        print "%s\t->\t%d) %s" % (torId, torRange[i], "".join(findall(r"[0-9]+(?:\.[0-9]+){3}", res)))
        i += 1
        
    #Change IP
    ipChanged = False
    while not ipChanged:
        ipChanged = torConn.changeIp(i, "Finished.")

    #Free The TorConnection
    torConn.changeState()

from crawlerGetAllPhotoIDs import CrawlerPhotoIDs
from crawlerGetItemInfo import ItemCrawler
from crawlerDownloadImg import download
import sys
import os

def crawl(torRange, author_id):
    # Important Variables
    torConn = torConnColl.getFreeConnection()
    proxies = torConn.getProxies()
    torId = torConn.getId()
    size = len(torRange)

    print "%s\t->\tStart (%d - %d)" % (torId, torRange[0], torRange[-1])


    log_file = 'istock_crawler_GetAllPhotoIDs.log'

    # ----------------------- list of photos --------------------------
    path = str(author_id) + '_gm_list.txt'
    print("author %d: getting list of photos" % int(author_id))
    i = 0
    # Using A While Loop - For Loop Cant Move Backwards
    while i < size:
        try:
            l = CrawlerPhotoIDs(author_id, log_file).get_id_list(save_to_file=True, save_filepath=path)
            break
        except Exception as ex:
            # Change IP
            ipChanged = False
            while not ipChanged:
                ipChanged = torConn.changeIp(torRange[i], ex)
            i += 1
            continue

    # Change IP
    ipChanged = False
    while not ipChanged:
        ipChanged = torConn.changeIp(i, "Finished.")

    # Free The TorConnection
    torConn.changeState()

    # ------------------------------ rest info ------------------------------
    i = 0
    print("author %d: getting photo info" % int(author_id))
    path_to_csv = str(author_id) + '.csv'
    while i < size:
        try:
            ItemCrawler(l).get_result(save_filepath=path_to_csv)
            break
        except Exception as ex:
            # Change IP
            ipChanged = False
            while not ipChanged:
                ipChanged = torConn.changeIp(torRange[i], ex)
            i += 1
            continue

    # Change IP
    ipChanged = False
    while not ipChanged:
        ipChanged = torConn.changeIp(i, "Finished.")

    # Free The TorConnection
    torConn.changeState()

    # ----------------------------------- download previews ---------------------
    i = 0
    print("author %d: downloading previews" % int(author_id))
    if not os.path.exists(str(author_id)):
        os.makedirs(str(author_id))
    while i < size:
        try:
            download(path_to_csv, save_to=(str(author_id) + '/'))
            break
        except Exception as ex:
            # Change IP
            ipChanged = False
            while not ipChanged:
                ipChanged = torConn.changeIp(torRange[i], ex)
                i += 1
            continue

    # Change IP
    ipChanged = False
    while not ipChanged:
        ipChanged = torConn.changeIp(i, "Finished.")

    # Free The TorConnection
    torConn.changeState()


def main():
    author_list = [2881663, 1010902, 475444]
    # author_list = [7473708, 9656223, 5018712, 13144499]
    #Kill All Tor Processes
    for process in process_iter():
        try:
            if path.basename(TOR_CMD) == process.name:
                process.terminate()
        except AccessDenied:
            continue

    #Extract Tor Windows Files If Needed
    if isWindows() and not path.exists(TOR_ROOT_DATA_PATH):
        makedirs(TOR_ROOT_DATA_PATH)
        ZipFile(path.join(getcwd(), "torWin.data")).extractall(TOR_ROOT_DATA_PATH)

    #Create TorConnectionCollector And Tor PassPhrase Hash
    global torConnColl, passPhraseHash
    passPhraseHash = check_output([TOR_CMD, "--hash-password", PASS_PHRASE]).strip().split("\n")[-1]
    torConnColl = TorConnectionCollector()

    #Create The Threads Pool
    #Look after INC*i + INC not being > END
    for i in xrange(0, len(author_list)):
        POOL.spawn(crawl, range(i*INC, INC*i + INC), author_list[i])

    #Block Until Pool Done
    POOL.join()

    #Kill All TorConnections
    print "Kill Tor Connections"
    torConnColl.killConnections()

    #Finish
    print "Finished Scanning"

if __name__ == "__main__":
    main()


